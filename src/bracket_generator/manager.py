import logging

from match.models import (
    Match,
    MatchTeam,
)
from team.models import Team


from settings.logging import CUSTOM_LOGGER_NAME


logger = logging.getLogger(CUSTOM_LOGGER_NAME)


class BracketGenerator:
    FAKE_TEAM = 'F'

    def __init__(self, tournament):
        self.tournament = tournament

    @staticmethod
    def get_rounds_count(teams_count):
        if teams_count == 0:
            return 0

        odd_number_of_teams = bool(teams_count % 2)

        if odd_number_of_teams:
            rounds_count = teams_count
        else:
            rounds_count = teams_count - 1

        return rounds_count

    @staticmethod
    def _get_team_pairs(current_teams):
        """
        Examples:
        ['1', '2', '3', '4'] --> [('1', '4'), ('2', '3')]
        ['1','2','3','4','5','F'] --> [('1', 'F'), ('2', '5'), ('3', '4')]

        :param current_teams: current team order for round
        :return: team id pairs for this round matches
        """

        pairs_count = int(len(current_teams) / 2)

        for team_index in range(pairs_count):
            yield current_teams[team_index], current_teams[- team_index - 1]

    def _match_has_fake_team(self, team_left, team_right):
        return (team_left == self.FAKE_TEAM) or (team_right == self.FAKE_TEAM)

    def generate_brackets(self):
        teams_map = {
            team.pk: team for team in Team.objects.filter(tournament=self.tournament)
        }

        team_ids = list(teams_map.keys())
        teams_count = len(team_ids)

        if teams_count < 2:
            raise Exception(f'Teams count must be GTE 2.')

        if teams_count % 2:
            team_ids.append(self.FAKE_TEAM)

        for i in range(self.get_rounds_count(teams_count)):
            for (team_left_id, team_right_id) in self._get_team_pairs(team_ids):
                if self._match_has_fake_team(team_left_id, team_right_id):
                    continue

                match = Match.objects.create(
                    tournament=self.tournament,
                    round=i + 1
                )

                for team_id in [team_left_id, team_right_id]:
                    MatchTeam.objects.create(
                        match=match,
                        team=teams_map[team_id]
                    )

            team_ids.insert(1, team_ids.pop())
