from enum import Enum

from django.db import models

from team.models import Team
from tournament.models import Tournament


class Match(models.Model):
    class STATUSES(Enum):
        INITIAL = 'Initial'
        IN_PROGRESS = 'In progress'
        COMPLETED = 'Completed'

    STATUS_CHOICES = [(status.name, status.value) for status in STATUSES]

    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    round = models.IntegerField()
    teams = models.ManyToManyField(Team, through='MatchTeam', related_name='matches')
    winner = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, default=None)
    status = models.CharField(max_length=32, choices=STATUS_CHOICES, default=STATUSES.INITIAL.name, db_index=True)

    class Meta:
        verbose_name_plural = 'matches'

    def __str__(self):
        return f'Match {self.tournament}'


class MatchTeam(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
