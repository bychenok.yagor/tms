from django.contrib import admin

from tournament.models import Tournament


class TournamentAdmin(admin.ModelAdmin):
    search_fields = ('name', )


admin.site.register(Tournament, TournamentAdmin)
