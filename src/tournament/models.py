from django.db import models


class Tournament(models.Model):
    name = models.CharField(max_length=32)
    start_at = models.DateTimeField(null=True)
    winner = models.ForeignKey('team.Team', on_delete=models.CASCADE, null=True,
                               default=None, related_name="tournament_winner")

    def __str__(self):
        return f'Tournament {self.name}'
