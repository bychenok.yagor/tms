import operator

from django.core.management.base import BaseCommand

from match.models import Match
from team.models import Team
from tournament.models import Tournament


def calculate_winner(tournament):
    matches = Match.objects.filter(tournament=tournament).exclude(winner=None)
    points = {}

    for match in matches:
        if match.winner.id in points:
            points[match.winner.id] += 1
        else:
            points[match.winner.id] = 1

    if points:
        winner_id = max(points.items(), key=operator.itemgetter(1))[0]
        winner = Team.objects.get(pk=winner_id)
        return winner
    else:
        return None


class Command(BaseCommand):
    help = 'Calculating winners for all tournaments'

    def handle(self, *args, **options):
        tournaments = Tournament.objects.filter(winner=None)
        for tournament in tournaments:
            winner_team = calculate_winner(tournament)

            tournament.winner = winner_team
            tournament.save()
