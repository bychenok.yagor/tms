import json

from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse


class TournamentsTest(TestCase):
    def test_create_tournament(self):
        name = 'test_create_tournament'
        payload = {
            'name': name,
        }

        response = self.client.post(
            path=reverse('tournament-list'),
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)

        response_jsn = json.loads(response.content)
        self.assertEqual(response_jsn['name'], name)
        self.assertIsNotNone(response_jsn['id'])
