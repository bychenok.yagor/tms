from django.db import models

from tournament.models import Tournament


class Team(models.Model):
    name = models.CharField(max_length=32)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)

    def __str__(self):
        return f'Team {self.name} in {self.tournament}'
