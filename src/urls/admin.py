from django.contrib import admin
from django.urls import path

__all__ = (
    'urlpatterns',
)


urlpatterns = [
    path('admin/', admin.site.urls),
]
