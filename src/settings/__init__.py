import os

from settings.base import *
from settings.databases import DATABASES
from settings.languages import *
from settings.templates import TEMPLATES
from settings.logging import *
from settings.cache import *

settings_local = os.environ.get('CATALOG_TOOL_SETTINGS_LOCAL_PATH', os.path.join(BASE_DIR, 'settings_local.py'))

if os.path.exists(settings_local):
    exec(open(settings_local).read())
