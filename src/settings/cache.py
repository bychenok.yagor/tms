__all__ = (
    'CACHES',
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'test_table_for_cache'
    }
}
