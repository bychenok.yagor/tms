from rest_framework import serializers

from tournament.models import Tournament


class TournamentSerializer(serializers.ModelSerializer):
    start_at = serializers.DateTimeField(required=False)

    class Meta:
        model = Tournament
        exclude = ()
