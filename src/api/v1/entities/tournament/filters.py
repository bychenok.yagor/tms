from django_filters import (
    CharFilter,
    FilterSet,
)


class TournamentFilter(FilterSet):
    name = CharFilter(lookup_expr='startswith')
