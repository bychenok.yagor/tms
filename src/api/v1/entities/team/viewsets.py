from rest_framework import viewsets

from api.v1.entities.team.serializers import TeamSerializer
from team.models import Team


class TeamViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
