from rest_framework import serializers

from match.models import Match


class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Match
        exclude = ()


class MatchWinnerSerializer(serializers.Serializer):
    winner_team_id = serializers.IntegerField()
